from sqlalchemy import Boolean, Column, Date, Float, ForeignKey, Integer, String, Table
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import NullType
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Artist(Base):
    __tablename__ = 'artist'

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)


class MediaType(Base):
    __tablename__ = 'media_type'

    id = Column(Integer, primary_key=True)
    name = Column(String)


class Role(Base):
    __tablename__ = 'role'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    mail = Column(String, unique=True)
    login = Column(String, unique=True)
    password = Column(String)
    phone = Column(String)
    address = Column(String)
    max_borrow_by_week = Column(Integer)

    roles = relationship('Role', secondary='users_roles')

    def as_dict(self):
       return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "mail": self.mail,
            "login": self.login,
            "password": self.password,
            "phone": self.phone,
            "address": self.address,
            "max_borrow_by_week": self.max_borrow_by_week
        }

class Media(Base):
    __tablename__ = 'media'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    available = Column(Boolean)
    grade = Column(Float)
    creation_date = Column(Date)
    id_media_type = Column(ForeignKey('media_type.id', ondelete='RESTRICT', onupdate='RESTRICT'))
    id_artist = Column(ForeignKey('artist.id', ondelete='RESTRICT', onupdate='RESTRICT'))

    artist = relationship('Artist')
    media_type = relationship('MediaType')


t_users_roles = Table(
    'users_roles', metadata,
    Column('id_user', ForeignKey('user.id', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True, nullable=False),
    Column('id_role', ForeignKey('role.id', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True, nullable=False)
)


class Borrow(Base):
    __tablename__ = 'borrow'

    id = Column(Integer, primary_key=True)
    id_user = Column(ForeignKey('user.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    id_media = Column(ForeignKey('media.id', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False)
    date_borrow = Column(Date, nullable=False)
    return_date_borrow = Column(Date)

    media = relationship('Media')
    user = relationship('User')