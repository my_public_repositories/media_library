''' tests for the media_library project '''
from media_library import __version__


def test_version():
    assert __version__ == '0.1.0'
