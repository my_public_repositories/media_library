-- import to SQLite by running: sqlite3.exe db.sqlite3 -init sqlite.sql


PRAGMA journal_mode = MEMORY;
PRAGMA synchronous = OFF;
PRAGMA foreign_keys = OFF;
PRAGMA ignore_check_constraints = OFF;
PRAGMA auto_vacuum = NONE;
PRAGMA secure_delete = OFF;


DROP TABLE IF EXISTS role ;
CREATE TABLE role (
    id  integer PRIMARY KEY, 
    name VARCHAR UNIQUE
);  
DROP TABLE IF EXISTS user ;
CREATE TABLE user (
    id integer PRIMARY KEY, 
    first_name VARCHAR, 
    last_name VARCHAR, 
    mail VARCHAR  UNIQUE, 
    login VARCHAR  UNIQUE, 
    password VARCHAR,
    phone VARCHAR, 
    address VARCHAR,
    max_borrow_by_week INTEGER
);
DROP TABLE IF EXISTS users_roles ;
CREATE TABLE users_roles (
    id_user integer  NOT NULL, 
    id_role integer NOT NULL, 
    PRIMARY KEY (id_user,  id_role),
    FOREIGN KEY (id_user) REFERENCES user (id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_role) REFERENCES role (id)  ON UPDATE CASCADE ON DELETE CASCADE
);
DROP TABLE IF EXISTS artist ;
CREATE TABLE artist (
    id integer PRIMARY KEY, 
    first_name VARCHAR, 
    last_name VARCHAR
);
DROP TABLE IF EXISTS media_type ;
CREATE TABLE media_type (
    id integer PRIMARY KEY, 
    name VARCHAR
);
DROP TABLE IF EXISTS media ;
CREATE TABLE media (
    id integer PRIMARY KEY,
    name VARCHAR, 
    available BOOLEAN,
    grade FLOAT,
    creation_date DATE,
    id_media_type integer, 
    id_artist integer, 
    FOREIGN KEY (id_media_type) REFERENCES media_type (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    FOREIGN KEY (id_artist) REFERENCES artist (id) ON UPDATE RESTRICT ON DELETE RESTRICT

);
DROP TABLE IF EXISTS borrow ;
CREATE TABLE borrow (
    id integer PRIMARY KEY,
    id_user integer NOT NULL, 
    id_media integer NOT NULL, 
    date_borrow date NOT NULL, 
    return_date_borrow date , 
    FOREIGN KEY (id_user) REFERENCES user (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    FOREIGN KEY (id_media) REFERENCES media (id)  ON UPDATE RESTRICT ON DELETE RESTRICT
);

PRAGMA ignore_check_constraints = ON;
PRAGMA foreign_keys = ON;
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;