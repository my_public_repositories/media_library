from datetime import datetime
from typing import List
from sqlalchemy.orm import Session
from sqlalchemy import and_
from scrapper import get_book_grade, get_video_grade
import utilities
import models, custom_schemas


# region role CRUD methods 

def get_all_roles(db: Session)->List[models.Role]:
    return db.query(models.Role).all()

def get_role(db: Session, id: int)->models.Role:
    return db.query(models.Role).get(id)

def get_role_by_name(db: Session, name: str)->models.Role:
    return db.query(models.Role).filter(models.Role.name == name).first()

def create_role(db: Session, role: custom_schemas.RoleCreate)->models.Role:
    db_role = models.Role(name=role.name)
    db.add(db_role)
    db.commit()
    db.refresh(db_role)
    return db_role

def update_role(db: Session, id: int, role: custom_schemas.RoleCreate)->models.Role:
    db.query(models.Role).filter_by(id=id).update({"name": role.name})
    db.commit()
    return db.query(models.Role).get(id)

def delete_role(db: Session, id: str)->str:
    role = db.query(models.Role).get(id)
    db.delete(role)
    db.commit()
    return 'role was deleted'

# endregion

# region media Type CRUD methods 

def get_media_types(db: Session)->List[models.MediaType]:
    return db.query(models.MediaType).all()

def get_media_type(db: Session, id: int)->models.MediaType:
    return db.query(models.MediaType).get(id)

def get_media_type_by_name(db: Session, name: str)->models.MediaType:
    return db.query(models.MediaType).filter(models.MediaType.name == name).first()

def create_media_type(db: Session, media_type: custom_schemas.MediaTypeCreate)->models.MediaType:
    db_media_type = models.MediaType(name=media_type.name)
    db.add(db_media_type)
    db.commit()
    db.refresh(db_media_type)
    return db_media_type

def update_media_type(db: Session, id: int, media_type: custom_schemas.MediaTypeCreate)->models.MediaType:
    db.query(models.MediaType).filter_by(id=id).update({"name": media_type.name})
    db.commit()
    return db.query(models.MediaType).get(id)

def delete_media_type(db: Session, id: str)->str:
    role = db.query(models.MediaType).get(id)
    db.delete(role)
    db.commit()
    return 'media type was deleted'

# endregion

# region user Type CRUD methods 


def get_users(db: Session)->List[models.User]:
    users = db.query(models.User).all()
    return users

def get_user(db: Session, id: int)->models.User:
    return db.query(models.User).get(id)

def get_user_by_login(db: Session, login: str)->models.User:
    return db.query(models.User).filter(models.User.login == login).first()

def get_user_by_mail(db: Session, mail: str)->models.User:
    return db.query(models.User).filter(models.User.mail == mail).first()

def create_user(db: Session, user: custom_schemas.UserCreate)->models.User:
    hashed = utilities.encrypt_password(user.password)
    db_user = models.User(login=user.login, mail=user.mail,first_name=user.first_name,last_name=user.last_name, phone=user.phone, address=user.address, max_borrow_by_week=3, password= hashed)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def update_user(db: Session, id: int, user: custom_schemas.UserUpdate)->models.User:
    user_query = db.query(models.User).filter_by(id=id)
    for k, v in user:
        if k == "password":
            user_query.update({k: utilities.encrypt_password(v)})
        else:
            user_query.update({k: v})
    db.commit()
    return db.query(models.User).get(id)

def delete_user(db: Session, id: str)->str:
    user = db.query(models.User).get(id)
    db.delete(user)
    db.commit()
    return 'user was deleted'

def add_role_to_user(db: Session, user: models.User, role: models.Role)->str:
    user.roles.append(role)
    db.commit()
    return user

def delete_user_role(db: Session, user: models.User, role: models.Role)->str:
    user.roles.remove(role)
    db.commit()
    return 'user role was deleted'

# endregion

# region artist CRUD methods 

def get_artists(db: Session)->List[models.Artist]:
    return db.query(models.Artist).all()

def get_artist(db: Session, id: int)->models.Artist:
    return db.query(models.Artist).get(id)

def get_artist_by_full_name(db: Session, first_name: str, last_name: str)->models.Artist:
    return db.query(models.MediaType).filter(and_(models.Artist.first_name == first_name, models.Artist.last_name == last_name)).first()

def create_artist(db: Session, artist: custom_schemas.ArtistCreate)->models.Artist:
    db_artist = models.Artist(first_name=artist.first_name, last_name=artist.last_name)
    db.add(db_artist)
    db.commit()
    db.refresh(db_artist)
    return db_artist

def update_artist(db: Session, id: int, artist: custom_schemas.Artist)->models.Artist:
    artist_query = db.query(models.Artist).filter_by(id=id)
    for k, v in artist:
        artist_query.update({k: v})
    db.commit()
    return db.query(models.Artist).get(id)

def delete_artist(db: Session, id: str)->str:
    artist = db.query(models.Artist).get(id)
    db.delete(artist)
    db.commit()
    return 'artist was deleted'

# endregion

# region media CRUD methods 

def get_medias(db: Session)->List[models.Media]:
    users = db.query(models.Media).all()
    return users

def get_media(db: Session, id: int)->models.Media:
    return db.query(models.Media).get(id)

def get_media_grade(db: Session, media_type_name: str, media_name: str)->float:
    db_media_type = get_media_type_by_name(db, media_type_name)
    grade = None
    if db_media_type.name=="dvd":
        grade = get_video_grade(media_name)
    elif db_media_type.name=="livre":
        grade = get_book_grade(media_name)
    return grade

def create_media(db: Session, media: custom_schemas.MediaCreate)->models.Media:
    db_media_type = get_media_type_by_name(db, media.media_type_name)
    grade = get_media_grade(db, media.media_type_name, media.name)
    db_media = models.Media(name = media.name, available = True, grade = grade, creation_date = media.creation_date, id_media_type = db_media_type.id, id_artist = media.id_artist)
    db.add(db_media)
    db.commit()
    db.refresh(db_media)
    return db_media

def update_media(db: Session, id: int, media: custom_schemas.MediaUpdate)->models.Media:
    db_media_type = get_media_type_by_name(db, media.media_type_name)
    grade = get_media_grade(db, media.media_type_name, media.name)
    media_query = db.query(models.Media).filter_by(id=id)
    for k, v in media:
        if k == "id_media_type":
            media_query.update({k:  db_media_type.id})
        elif k == "media_type_name":
            pass
        else:
            media_query.update({k: v})
    media_query.update({"grade":  grade})
    db.commit()
    return db.query(models.Media).get(id)

def delete_media(db: Session, id: str)->str:
    media = db.query(models.Media).get(id)
    db.delete(media)
    db.commit()
    return 'media was deleted'

# endregion

# region borrow CRUD methods 

def get_borrows(db: Session)->List[models.Borrow]:
    borrows = db.query(models.Borrow).all()
    return borrows

def get_borrow(db: Session, id)->models.Borrow:
    return db.query(models.Borrow).get(id)

def create_borrow(db: Session, borrow: custom_schemas.BorrowCreate)->models.Borrow:
    db_borrow = models.Borrow(id_user = borrow.id_user, id_media = borrow.id_media, date_borrow = datetime.today().date())
    db.query(models.Media).filter_by(id=borrow.id_media).update({"available": False})
    db.add(db_borrow)
    db.commit()
    db.refresh(db_borrow)
    return db_borrow

def update_borrow(db: Session, id: int, borrow: custom_schemas.BorrowUpdate)->models.Borrow:
    borrow_query = db.query(models.Borrow).filter_by(id=id)
    for k, v in borrow:
        borrow_query.update({k: v})
    db.commit()
    return db.query(models.Borrow).get(id)

def delete_borrow(db: Session, id: str)->str:
    borrow = db.query(models.Borrow).get(id)
    db.delete(borrow)
    db.commit()
    return 'borrow was deleted'

# endregion