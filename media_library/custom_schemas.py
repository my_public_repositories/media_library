''' defining schemas that objects should respect '''
from sqlite3 import Date
from tokenize import String
from typing import List, Union
from xmlrpc.client import boolean
from pydantic import BaseModel, validator
from datetime import date, datetime


# region roles schemas

class RoleBase(BaseModel):
    name : str


class RoleCreate(RoleBase):
    pass


class RoleUpdate(RoleBase):
    pass


class Role(RoleBase):
    id : int

    class Config:
        orm_mode : True


# endregion


# region media type schemas

class MediaTypeBase(BaseModel):
    name : str


class MediaTypeCreate(MediaTypeBase):
    pass


class MediaTypeUpdate(MediaTypeBase):
    pass


class MediaType(MediaTypeBase):
    id : int

    class Config:
        orm_mode : True


# endregion


# region user schema

class UserBase(BaseModel):
    mail: str
    first_name : str
    last_name : str
    login : str
    phone : str
    address : str


class UserCreate(UserBase):
    password: str


class UserUpdate(UserCreate):
    pass

class UserAuth(UserCreate):
    id : int
    max_borrow_by_week : int

class User(UserAuth):

    roles :List[Role]

    class Config:
        orm_mode : True

# endregion


# region artist schemas

class ArtistBase(BaseModel):
    first_name : str
    last_name : str


class ArtistCreate(ArtistBase):
    pass


class ArtistUpdate(ArtistBase):
    pass


class Artist(ArtistBase):
    id : int

    class Config:
        orm_mode : True

# endregion


# region media schema

class MediaBase(BaseModel):
    name: str
    creation_date: date
    id_artist: int
    media_type_name: str

    @validator("creation_date", pre=True)
    def parse_creation_date(cls, value):
        return datetime.strptime(value,"%d/%m/%Y").date()


class MediaCreate(MediaBase):
    pass


class MediaUpdate(MediaCreate):
    available: boolean
    pass


class Media(MediaUpdate):
    id : int
    grade: float

    class Config:
        orm_mode : True


# endregion


# region borrow schema

class BorrowBase(BaseModel):
    id_user: int
    id_media: int


class BorrowCreate(BorrowBase):
    pass


class BorrowUpdate(BorrowBase):
    date_borrow: date
    return_date_borrow: date

    @validator("date_borrow", pre=True)
    def parse_date_borrow(cls, value):
        return datetime.strptime(value,"%d/%m/%Y").date()

    @validator("return_date_borrow", pre=True)
    def parse_return_date_borrow(cls, value):
        return datetime.strptime(value,"%d/%m/%Y").date()


class Borrow(BorrowUpdate):
    id: int
    
    class Config:
        orm_mode : True


# endregion


# region token schema


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Union[str, None] = None


# endregion