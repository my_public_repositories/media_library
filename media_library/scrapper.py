from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from urllib.parse import quote
import json

book_root_url = "https://www.goodreads.com/"
video_internal_url = "https://v3.sg.media-imdb.com/suggestion/x/"
video_root_url = "https://www.imdb.com/"

def get_book_grade(book_name: str)->float:
    ''' get book grade from goodreads.com '''
    url = f"{book_root_url}search?query={quote(book_name)}"
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    soup = BeautifulSoup(webpage, 'html.parser')
    full_grade_text = soup.find("span", class_="minirating").contents[-1]
    grade = full_grade_text.split(' avg')[0]
    return 2 * float(grade)

# print(get_book_grade("le seigneur des anneaux"))

def get_video_grade(video_name: str)->float:
    ''' get video grade from imdb.com '''
    info_url = f"{video_internal_url}{quote(video_name)}.json"
    response = urlopen(info_url)
    data_json = json.loads(response.read())
    film_id = data_json["d"][0]["id"]
    url = f"{video_root_url}/title/{film_id}/"
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    soup = BeautifulSoup(webpage, 'html.parser')
    full_grade_text = soup.find("span", class_="sc-7ab21ed2-1").contents[0]
    return float(full_grade_text)

# print(get_video_grade("heat"))