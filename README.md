# media_library

Test project for shadow.

## used softwares
Design use AnalyseSI for merise MCD model and starUML for UML diagrams. 

## classes generation
I used sqlacodegen to generate classes:
`sqlacodegen sqlite:///media_library.db`

## followed tutorials
https://betterprogramming.pub/my-first-crud-app-with-fast-api-74ac190d2dcc 

## R&D
Look at easyauth: https://itnext.io/creating-secure-apis-with-easyauth-fastapi-6996a5e42d07
pydantic: custom json encoder https://codeutility.org/python-how-to-change-date-format-in-pydantic-stack-overflow/ 
full tuto: https://christophergs.com/tutorials/ultimate-fastapi-tutorial-pt-10-auth-jwt/  
TODO: activate https

research full text with sqlite: https://www.sqlitetutorial.net/sqlite-full-text-search/