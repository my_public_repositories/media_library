''' media_library main app '''
import os

import custom_schemas, models, crud
from database import Base, engine, SessionLocal

from fastapi import FastAPI, Depends, HTTPException, status
from sqlalchemy.orm import Session
from datetime import datetime, timedelta
from typing import Union
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext

# This will create our database if it doesent already exists
Base.metadata.create_all(engine)
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


app = FastAPI()

# region authentication

# to get a string like this run: openssl rand -hex 32
SECRET_KEY = os.getenv('SECRET_KEY')
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def verify_password(plain_password, hashed_password)->bool:
    return pwd_context.verify(plain_password, hashed_password)

def get_password_hash(password)->str:
    return pwd_context.hash(password)

def get_schema_user(db: Session, login: str)->custom_schemas.UserAuth:
    user: models.User = crud.get_user_by_login(db, login)
    if user:
        user_dict = user.as_dict()
        return custom_schemas.UserAuth(**user_dict)

def authenticate_user(db: Session, login: str, password: str)->custom_schemas.UserAuth:
    user = get_schema_user(db, login)
    if not user:
        return False
    if not verify_password(password, user.password):
        return False
    return user

def create_access_token(data: dict, expires_delta: Union[timedelta, None] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        login: str = payload.get("sub")
        if login is None:
            raise credentials_exception
        token_data = custom_schemas.TokenData(username=login)
    except JWTError:
        raise credentials_exception
    user = get_schema_user(db, login=token_data.username)
    if user is None:
        raise credentials_exception
    return user

async def is_admin(db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    current_user = await get_current_user(db,token)
    roles = crud.get_user(db, current_user.id).roles
    db.close()
    for role in roles:
        if role.name == "administrator":
            return True  
    raise HTTPException(status_code=400, detail="Not enough privileges")

@app.post("/token", response_model=custom_schemas.Token)
async def login_for_access_token( db: Session = Depends(get_db), form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect login or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.login}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}
    
@app.get("/users/me/")
async def read_users_me(current_user: custom_schemas.UserAuth = Depends(get_current_user)):
    return current_user

# endregion 

# region roles API routes   
            
@app.get("/role_admin/")
def get_roles(db: Session = Depends(get_db), is_admin: str = Depends(is_admin)):
    ''' get all roles'''
    return crud.get_all_roles(db)

@app.get("/role/")
def get_roles( db: Session = Depends(get_db)):
    ''' get all roles'''
    return crud.get_all_roles(db)

@app.get("/role/{role_id}")
def get_role(role_id: int, db: Session = Depends(get_db)):
    ''' get a specific role by id'''
    return crud.get_role(db, role_id)

@app.post("/role/")
def add_role(role: custom_schemas.RoleCreate, db: Session = Depends(get_db)):
    ''' add role method based on Body json request'''
    db_role = crud.get_role_by_name(db, name=role.name)
    if db_role:
        raise HTTPException(status_code=400, detail="Role already exists")
    return crud.create_role(db=db, role=role)

@app.put("/role/{role_id}")
def update_role(role_id: int, role_data: custom_schemas.RoleUpdate, db = Depends(get_db)):
    ''' update role method based on Body json request'''
    db_role = crud.get_role(db, id=role_id)
    if not db_role:
        raise HTTPException(status_code=400, detail="Role does not exist")
    return crud.update_role(db=db, id=role_id, role=role_data)
    
@app.delete("/role/{role_id}")
def delete_role(role_id: int, db = Depends(get_db)):
    ''' delete role method based on role id'''
    db_role = crud.get_role(db, id=role_id)
    if not db_role:
        raise HTTPException(status_code=400, detail="Role does not exist")
    return crud.delete_role(db=db, id=role_id)

# endregion


# region media type API routes   
            
@app.get("/media_type/")
def get_media_types(db: Session = Depends(get_db)):
    ''' get all media_type'''
    return crud.get_media_types(db)

@app.get("/media_type/{media_type_id}")
def get_media_type(media_type_id: int, db: Session = Depends(get_db)):
    ''' get a specific media_type by id'''
    return crud.get_media_type(db, media_type_id)

@app.post("/media_type/")
def add_media_type(media_type_data: custom_schemas.MediaTypeCreate, db: Session = Depends(get_db)):
    ''' add media_type method based on Body json request'''
    db_media_type = crud.get_media_type_by_name(db, name=media_type_data.name)
    if db_media_type:
        raise HTTPException(status_code=400, detail="MediaType already exists")
    return crud.create_media_type(db=db, media_type=media_type_data)

@app.put("/media_type/{media_type_id}")
def update_media_type(media_type_id: int, media_type_data: custom_schemas.MediaTypeUpdate, db = Depends(get_db)):
    ''' update media_type method based on Body json request'''
    db_media_type = crud.get_media_type(db, id=media_type_id)
    if not db_media_type:
        raise HTTPException(status_code=400, detail="MediaType does not exist")
    return crud.update_media_type(db=db, id=media_type_id, media_type=media_type_data)
    
@app.delete("/media_type/{media_type_id}")
def delete_media_type(media_type_id: int, db = Depends(get_db)):
    ''' delete media_type method based on media_type id'''
    db_media_type = crud.get_media_type(db, id=media_type_id)
    if not db_media_type:
        raise HTTPException(status_code=400, detail="MediaType does not exist")
    return crud.delete_media_type(db=db, id=media_type_id)

# endregion


# region users API routes

@app.get("/user/")
def get_users(db: Session = Depends(get_db)):
    ''' get all users'''
    return crud.get_users(db)

@app.get("/user/{user_id}")
def get_user(user_id: int, db: Session = Depends(get_db)):
    ''' get a specific user by id'''
    return crud.get_user(db, user_id)

@app.post("/user/")
def add_user(user_data:custom_schemas.UserCreate, db = Depends(get_db)):
    ''' add user method based on Body json request'''
    db_user = crud.get_user_by_login(db, user_data.login)
    if db_user:
        raise HTTPException(status_code=400, detail="User with same login already exists")
    db_user = crud.get_user_by_mail(db, user_data.mail)
    if db_user:
        raise HTTPException(status_code=400, detail="User with same mail already exists")
    return crud.create_user(db=db, user=user_data)

@app.put("/user/{user_id}")
def update_user(user_id:int, user_data:custom_schemas.UserCreate, db = Depends(get_db)):
    ''' update user method based on Body json request'''
    db_user = crud.get_user(db, id=user_id)
    if not db_user:
        raise HTTPException(status_code=400, detail="User does not exist")
    return crud.update_user(db=db, id=user_id, user=user_data)

@app.delete("/user/{user_id}")
def delete_user(user_id:int, db = Depends(get_db)):
    ''' delete user method based on user id'''
    db_user = crud.get_media_type(db, id=user_id)
    if not db_user:
        raise HTTPException(status_code=400, detail="MediaType does not exist")
    return crud.delete_user(db=db, id=user_id)

@app.post("/user/{user_id}/role")
def add_user_role(user_id:int, role_data:custom_schemas.RoleBase, db = Depends(get_db)):
    ''' add a role to a specific user method based on Body json request'''
    db_user = crud.get_user(db,user_id)
    if not db_user:
        raise HTTPException(status_code=400, detail="User does not exist")
    db_role = crud.get_role_by_name(db,role_data.name)
    if not db_role:
        raise HTTPException(status_code=400, detail="Role does not exist")
    if db_role in db_user.roles:
        raise HTTPException(status_code=400, detail="User already get this role")
    return crud.add_role_to_user(db=db, user=db_user, role=db_role)

@app.get("/user/{user_id}/role")
def get_user_role(user_id: int, db: Session = Depends(get_db)):
    ''' get a specific user by id'''
    user = crud.get_user(db,user_id)
    if not user:
        raise HTTPException(status_code=400, detail="User does not exist")
    return user.roles

@app.delete("/user/{user_id}/role")
def delete_user(user_id:int, role_data:custom_schemas.RoleBase, db = Depends(get_db)):
    ''' delete user method based on user id'''
    user = crud.get_user(db, id=user_id)
    if not user:
        raise HTTPException(status_code=400, detail="User does not exist")
    role = crud.get_role_by_name(db,role_data.name)
    if not role:
        raise HTTPException(status_code=400, detail="Role does not exist")
    role = crud.get_role_by_name(db,role_data.name)
    if role not in user.roles:
        raise HTTPException(status_code=400, detail="User does not owe this role")
    return crud.delete_user_role(db=db, user=user, role=role)

# endregion


# region artist type API routes   
            
@app.get("/artist/")
def get_artists(db: Session = Depends(get_db)):
    ''' get all artists'''
    return crud.get_artists(db)

@app.get("/artist/{id_artist}")
def get_artist(id_artist: int, db: Session = Depends(get_db)):
    ''' get a specific artist by id'''
    return crud.get_artists(db, id_artist)

@app.post("/artist/")
def add_artist(artist_data: custom_schemas.ArtistCreate, db: Session = Depends(get_db)):
    ''' add artist method based on Body json request'''
    db_artist = crud.get_artist_by_full_name(db, first_name=artist_data.first_name, last_name=artist_data.last_name)
    if db_artist:
        raise HTTPException(status_code=400, detail="Artist already exists!")
    return crud.create_artist(db=db, artist=artist_data)

@app.put("/artist/{id_artist}")
def update_artist(id_artist: int, artist_data: custom_schemas.ArtistUpdate, db = Depends(get_db)):
    ''' update artist method based on Body json request'''
    db_artist = crud.get_artist(db, id=id_artist)
    if not db_artist:
        raise HTTPException(status_code=400, detail="Artist does not exist!")
    return crud.update_artist(db=db, id=id_artist, artist=artist_data)
    
@app.delete("/artist/{id_artist}")
def delete_artist(id_artist: int, db = Depends(get_db)):
    ''' delete artist method based on artist id'''
    db_artist = crud.get_artist(db, id=id_artist)
    if not db_artist:
        raise HTTPException(status_code=400, detail="Artist does not exist")
    return crud.delete_artist(db=db, id=id_artist)

# endregion


# region media API routes

@app.get("/media/")
def get_medias(db: Session = Depends(get_db)):
    ''' get all medias'''
    return crud.get_medias(db)

@app.get("/media/{media_id}")
def get_media(media_id: int, db: Session = Depends(get_db)):
    ''' get a specific media by id'''
    return crud.get_media(db, media_id)

@app.post("/media/")
def add_media(media_data:custom_schemas.MediaCreate, db = Depends(get_db)):
    ''' add media method based on Body json request'''
    db_artist = crud.get_artist(db, media_data.id_artist)
    if not db_artist:
        raise HTTPException(status_code=400, detail="Artist for this media does not exist")
    db_media_type = crud.get_media_type_by_name(db, media_data.media_type_name)
    if not db_media_type:
        raise HTTPException(status_code=400, detail="Media Type for this media does not exist")
    return crud.create_media(db=db, media=media_data)

@app.put("/media/{media_id}")
def update_user(media_id:int, media_data:custom_schemas.MediaUpdate, db = Depends(get_db)):
    ''' update media method based on Body json request'''
    db_media = crud.get_user(db, id=media_id)
    if not db_media:
        raise HTTPException(status_code=400, detail="Media does not exist")
    return crud.update_media(db=db, id=media_id, borrow=media_data)

@app.delete("/media/{media_id}")
def delete_user(media_id:int, db = Depends(get_db)):
    ''' delete media method based on media id'''
    db_media = crud.get_media_type(db, id=media_id)
    if not db_media:
        raise HTTPException(status_code=400, detail="Media does not exist")
    return crud.delete_media(db=db, id=media_id)

# endregion


# region borrow API routes

@app.get("/borrow/")
def get_borrows(db: Session = Depends(get_db)):
    ''' get all borrows'''
    return crud.get_borrows(db)

@app.get("/borrow/{borrow_id}")
def get_borrow(borrow_id: int, db: Session = Depends(get_db)):
    ''' get a specific borrow by id'''
    return crud.get_media(db, borrow_id)

@app.post("/borrow/")
def add_borrow(borrow_data:custom_schemas.BorrowCreate, db = Depends(get_db)):
    ''' add borrow method based on Body json request'''
    db_user = crud.get_user(db, borrow_data.id_user)
    if not db_user:
        raise HTTPException(status_code=400, detail="User does not exist")
    db_media = crud.get_media(db, borrow_data.id_media)
    if not db_media:
        raise HTTPException(status_code=400, detail="Media does not exist")
    if not db_media.available:
        raise HTTPException(status_code=400, detail="Media already borrowed")
    return crud.create_borrow(db=db, borrow=borrow_data)

@app.put("/borrow/{borrow_id}")
def update_borrow(borrow_id:int, borrow_data:custom_schemas.BorrowUpdate, db = Depends(get_db)):
    ''' update media method based on Body json request'''
    db_borrow = crud.get_user(db, id=borrow_id)
    if not db_borrow:
        raise HTTPException(status_code=400, detail="Borrow does not exist")
    if borrow_data.date_borrow > borrow_data.return_date_borrow:
        raise HTTPException(status_code=400, detail="The return date cannot be less than the borrowing date")
    return crud.update_borrow(db=db, id=borrow_id, borrow = borrow_data)

@app.delete("/borrow/{borrow_id}")
def delete_borrow(borrow_id:int, db = Depends(get_db)):
    ''' delete media method based on media id'''
    db_borrow = crud.get_borrow(db, id=borrow_id)
    if not db_borrow:
        raise HTTPException(status_code=400, detail="Borrow does not exist")
    return crud.delete_borrow(db=db, id=borrow_id)

# endregion


