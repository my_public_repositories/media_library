import bcrypt

def encrypt_password(password: str):
    # Encrypt the pasword to store:
    return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt(10)) 